<?php

namespace Farhang\Marque\Tests;

use Farhang\Marque\MarqueServiceProvider;
use Orchestra\Testbench\TestCase;

abstract class MarqueTestCase extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [MarqueServiceProvider::class];
    }
}
