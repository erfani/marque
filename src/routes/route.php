<?php
    Route::get('/', ['as' => 'marque.index', 'uses' => 'MarqueController@getIndex']);
    Route::get('{any}', ['uses' => 'MarqueController@getIndex'])->where('any', '.*');
    Route::delete('{any}', ['as' => 'marque.delete-content', 'uses' => 'MarqueController@deletePage'])->where('any', '.*');
    Route::put('{any}', ['as' => 'marque.update-content', 'uses' => 'MarqueController@editPage'])->where('any', '.*');
    Route::post('{any?}', ['as' => 'marque.add-content', 'uses' => 'MarqueController@addPage'])->where('any', '.*');
