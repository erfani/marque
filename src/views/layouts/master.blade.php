<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/styles/zenburn.min.css">
        <link rel="stylesheet" type="text/css" href="{{ marque_path() }}/css/farhang/marque/marque.css">
        <link rel="stylesheet" type="text/css" href="{{ marque_path() }}/css/farhang/marque/bootswatch.css">

        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="{{ marque_path() }}/js/farhang/marque/editor.js"></script>

        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/highlight.min.js"></script>
        <script type="text/javascript" src="{{ marque_path() }}/js/farhang/marque/marque.js"></script>
    </head>

    <body>
        @yield('content')
    </body>
</html>
