@extends('marque::layouts.master')

@section('content')
<script type="text/javascript">
    var prefixUrl = "{{ $root }}";
</script>
<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="ajax navbar-brand" href="{{ url(Config::get('marque.uri')) }}">{{ Config::get('marque.app_name') }}</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul id="navigation" class="nav navbar-nav multi-level">
            @include('marque::partials.nav', ['navBar' => $navBar, 'root' => $root])
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <img class="ajax-loader" src="img/farhang/marque/ajax-load.gif" />
            </li>
            <li>
                <a title="Logout" href="{{ url('auth/logout') }}">
                    <i class="glyphicon glyphicon-share-alt"></i>
                    <span class="text">Logout</span>
                </a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="content">
        @include('marque::controllers.marque.content', ['breadcrumbs' => $breadcrumbs, 'content' => $content, 'files' => $files, 'directories' => $directories])
    </div>
</div>
@stop
