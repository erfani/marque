<?php

namespace Farhang\Marque\Exceptions;

use Exception;

class ContentNotFoundException extends Exception
{
}
