<?php

namespace Farhang\Marque\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Farhang\Marque\Facades\Marque;
use Farhang\Marque\Exceptions\ContentNotFoundException;
use Config;
use Request;

class MarqueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex($any = '')
    {
        // Create the marque content directory if it does not exists
        if (Storage::exists('marque') === false) {
            Storage::makeDirectory('marque');
        }

        $isAjax = Request::ajax();
        $askRaw = Request::input('raw');
        $refreshNavBar = Request::input('nav');

        // Marque prefix uri
        $prefix = Config::get('marque.uri');

        // Get the breadcrumbs
        $breadcrumbs = Marque::getBreadCrumbs($any, $prefix);

        $notOnIndex = $any !== '';
        $any = 'marque/'.$any;
        $isFile = is_file(storage_path().'/app/'.$any);

        if (isset($askRaw) && $isFile) {
            $content = Marque::getRawPageContent($any);

            if ($isAjax === true) {
                return response()->json([
                    'content' => $content,
                    'title' => explode('.', basename($any))[0],
                    'isFile' => $isFile
                ]);
            }
        }

        // Get html formatted content
        $content = Marque::getPageContent($any);
        $directoryContent = Marque::getDirectoryContent($any, $prefix);


        if ($isAjax === true) {
            // Ajax request, return content without layout
            $html = view('marque::controllers.marque.content', [
                'files' => $directoryContent['files'],
                'directories' => $directoryContent['directories'],
                'content' => $content,
                'breadcrumbs' => $breadcrumbs,
                'notOnIndex' => $notOnIndex,
                'isFile' => $isFile
            ])->render();

            $navBarView = null;
            if (isset($refreshNavBar)) {
                $navBar = Marque::getNavBar(storage_path().'/app/marque');
                $navBarView = view('marque::partials.nav', [
                    'navBar' => $navBar,
                    'root' => $prefix
                ])->render();
            }

            return response()->json([
                'html' => $html,
                'navBarView' => $navBarView,
            ]);
        } else {
            $navBar = Marque::getNavBar(storage_path().'/app/marque');
        }

        return view('marque::controllers.marque.index', [
            'files' => $directoryContent['files'],
            'directories' => $directoryContent['directories'],
            'content' => $content,
            'breadcrumbs' => $breadcrumbs,
            'navBar' => $navBar,
            'root' => $prefix,
            'notOnIndex' => $notOnIndex,
            'isFile' => $isFile
        ]);
    }

    /**
     * Delete content
     * @param  string $any path
     */
    public function deletePage($any = '')
    {
        $deleted = false;
        $any = 'marque/'.$any;

        try {
            Marque::checkContent($any);
            $deleted = Marque::deleteContent($any);
            $message = 'Page deleted with success.';
        } catch (ContentNotFoundException $e) {
            $message = 'The page does not exists.';
        }

        return response()->json([
            'success' => $deleted,
            'message' => $message,
        ]);
    }

    /**
     * Update page content
     * @param  string $any path
     */
    public function editPage($any = '')
    {
        $content = Request::input('content');
        $name = Request::input('name');
        $success = false;
        $newPath = '';
        $filePath = dirname($any);

        try {
            $any = 'marque/'.$any;
            Marque::checkContent($any);

            $isFile = is_file(storage_path().'/app/'.$any);
            $currentDirectoryPath = dirname($any);

            $pageExists = Marque::checkIfPageExists($name, $currentDirectoryPath, $isFile ? 'file' : 'dir');
            if ($isFile === true) {
                $oldName = explode('.', basename($any))[0];
            } else {
                $explode = explode('/', $any);
                $oldName = $explode[count($explode) - 1];
            }

            if ($oldName !== $name && $pageExists === true) {
                $message = "A page with this name already exists.";
            } else {
                if ($isFile) {
                    // rename the markdown file
                    if ($oldName !== $name) {
                        Storage::put($currentDirectoryPath.'/'.$name.'.md', Marque::getRawPageContent($any));
                        Storage::delete($any);
                        $any = $newPath = $filePath.'/'.$name.'.md';
                    } else {
                        Storage::put($any, $content);
                    }

                    $success = true;
                    $content = Marque::getPageContent($any);
                    $message = 'Page updated with successs.';
                } else {
                    $success = rename(storage_path().'/app/'.$any, storage_path().'/app/'.$currentDirectoryPath.'/'.$name);
                    $newPath = $filePath.'/'.$name;
                    $message = 'Page updated with successs.';
                }
            }
        } catch (ContentNotFoundException $e) {
            $message = 'The page does not exists.';
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'content' => $content,
            'newPath' => $newPath
        ]);
    }

    /**
     * Add a page
     * @param string $any current directory path
     */
    public function addPage($any = '')
    {
        $name = Request::input('name');
        $type = Request::input('type');

        $success = false;

        if (empty($name)) {
            return response()->json([
                'success' => $success,
                'message' => 'Title cannot be empty.',
            ]);
        }

        $newPath = $any !== '/' ? $any : '';
        $any = 'marque/'.$any;
        $exists = Marque::checkIfPageExists($name, $any, $type);

        if ($exists === false) {
            if ($type === 'file') {
                $success = Storage::put($any.'/'.$name.'.md', '');
            } else {
                $success = Storage::makeDirectory($any.'/'.$name);
            }
        } else {
            $message = 'A page with this name already exists';
        }

        if ($success === true) {
            $message = 'Page created with success.';
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'newPath' => $newPath
        ]);
    }
}

