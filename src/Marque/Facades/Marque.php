<?php

namespace Farhang\Marque\Facades;

use Illuminate\Support\Facades\Facade;

class Marque extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'marque';
    }
}

