<?php 

namespace Farhang\Marque;

use Illuminate\Support\ServiceProvider;
use Farhang\Marque\Commands\FooCommand;

class MarqueServiceProvider extends ServiceProvider {

    protected $packageName = 'marque';

    protected $commands = [
        MarqueCommand::class;
    ];

    public function boot()
    {

        $this->app->singleton('marque', function () {
            return new Marque();
        });

        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

        $this->loadViewsFrom(__DIR__.'/../views', $this->packageName);

        $this->publishes([
            __DIR__.'/../assets' => public_path('mojo/'.$this->packageName),
        ], 'public');

        $this->publishes([
            __DIR__.'/../config/config.php' => config_path($this->packageName.'.php'),
        ], 'config');

        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', $this->packageName
        );

    }

}
