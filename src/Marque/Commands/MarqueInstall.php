<?php 

namespace Farhang\Marque\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class MarqueInstall extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'marque:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install marque';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $this->call('vendor:publish');
    }
}
